---
layout: post.html
title: Rendimiento I/O de mis VPS
tags: [VPS]
---

Bueno acabo de colocar el blog en un nuevo vps, por haber encontrado un mejor precio (y también porque me gusta probar diferentes empresas). Al parecer a muchos les gusta ver el rendimiento de los discos de los VPS de todas las empresas.

Así que aquí les coloco los resultados de un test al disco:

_Un VPS que tengo en [RocketvPS](http://www.rocketvps.com/)_

<pre>
<code>dd if=/dev/zero of=test bs=64k count=16k conv=fdatasync
16384+0 records in
16384+0 records out
1073741824 bytes (1.1GB) copied, 3.39703 s, 316 MB/s</code>
</pre>

_Aquí el VPS en donde acabo de colocar el blog recientemente_

<pre>
<code>dd if=/dev/zero of=test bs=64k count=16k conv=fdatasync
16384+0 records in
16384+0 records out
1073741824 bytes (1.1 GB) copied, 3.96514 s, 271 MB/s</code>
</pre>

Se ve la disminución de rendimiento, y será más bajo conforme vayan aumentando los sitios web. Sin embargo vale la pena por el precio (5$/mes), y en cuanto al servicio hasta ahora no he tenido muchos problemas.