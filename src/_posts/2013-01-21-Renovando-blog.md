---
layout: post.html
title: Renovando blog!
tags: [blog]
---

Como dice el título de la entrada, pueden ver que las cosas han cambiado un poco por aquí. Me encontraba probando unas configuraciones en unos VPS que compré estos últimos días, y me decidí por tener uno en el que pueda probar python (mi lenguaje favorito) sin las limitaciones que se tienen en la gran mayoría de servicios de almacenamiento.

Es así que luego de muchas reinstalaciones de SO, pude servir sitios hechos en django, webpy, web2py, tornado. Lo cual me tiene muy contento; sin embargo, en la búsqueda de información sobre estos paquetes, me topé con algo que lleva ya tiempo de moda entre desarrolladores de rails: Los generadores de sitios estáticos.

Me encantó la idea de generar sitios estáticos utilizando un sistema de plantillas, como lo hacen hoy en día casi todos los frameworks; así que me puse a buscar uno que esté escrito obviamente en python. Excelente fue encontrar muchas opciones para elegir, y es así que me decidí por mynt.

Bueno ya estaré colocando información acerca de [mynt](http://mynt.mirroredwhite.com/) y configuraciones, desarrollo y diseño de sitios utilizando estos generadores en las próximas entradas; así como haciendo cambios al diseño del blog.

¡Un abrazo!.