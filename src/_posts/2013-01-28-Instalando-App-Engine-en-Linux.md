---
layout: post.html
title: Instalando App Engine en Linux
tags: [web]
---
Hoy les escribiré sobre App Engine, un servicio de alojamiento de sitios web que nos ofrece Google gratuitamente; nace para ayudar a desarrolladores que no pueden obtener con facilidad un servicio para almacenar sus proyectos, en muchos casos por la falta de capital inicial.

Al registrarnos en su sitio, nos da la posibilidad de crear hasta 10 proyectos/aplicaciones, los cuales cuentan con determinadas cuotas, que para la mayoría de casos es más que suficiente, y si necesitarámos más tenemos la posibilidad de comprar mayores recursos a precios realmente bajos y accesibles.

Algo que hay que resaltar es que este servicio sólo da soporte para ciertos lenguajes de programación, los cuales son: Python, Java y Go (en el cual Google se encuentra trabajando buen tiempo y que aún es experimental). Para los fans de Java existe un plugin para Eclipse; sin embargo para esta publicación trabajaremos con el SDK (Kit de desarrollo) para Python.

Bueno lo primero que tenemos que hacer es ingresar al sitio de descarga de Google App Engine:

<pre><code>https://developers.google.com/appengine/downloads?hl=es</code></pre>

_También hay que tener instalado Python en nuestro sistema operativo, en el caso de Linux, este ya viene instalado (la versión con la cual trabaja App Engine es la 2.7.3)._

Ahora descargamos el paquete de instalación para Linux, lo cual nos descargará un .zip:

![Descargar e intalar SDK][downloadsdk]

Esperamos a que descargue el .zip y lo ubicamos:

![Paquete de instalación][zip]

Extraemos el archivo mediante "click derecho" o también lo podemos hacer utilizando la terminal:

<pre><code>$ unzip archivo.zip</code></pre>

Una vez termine el proceso se descomprensión del archivo colocamos lo siguiente en nuestra terminal:

<pre><code>$ export PATH=$PATH:/home/pavel/proyectos/google_appengine/</code></pre>

Esto vamos a tener que hacer por cada sesión y/o terminal que abrimos, en este caso donde dice "/pavel/" iría su nombre de usuario; yo lo he querido ubicar en la carpeta proyectos.

Luego de esto ya podemos empezar a desarrollar nuestra primera aplicación:

1. Nos ubicamos dentro de la carpeta "google_appengine"
2. Creamos la carpeta "apps", aquí irán nuestras aplicaciones.
3. Creamos una nueva carpeta llamada "test", donde crearemos dos nuevos archivos app.yaml y code.py (el primero es una archivo de configuración para App Engine y el segundo contiene el código fuente de nuestra pequeña aplicación.

Les dejo unas capturas de todo lo que contiene cada archivo:

![App.yaml][appyaml]

![code.py][codepy]

Luego de tener estos dos archivos dentro de la carpeta "test", podemos ver el resultado en nuestro localhost, escribiendo lo siguiente en nuestra terminal:

_Hay que ubicarnos en la carpeta "apps"_

<pre><code>$ dev_appserver.py test</code></pre>

Ahora abrimos nuestro navegador y entramos a:

<pre><code>http://localhost:8080</code></pre>

![localhost:8080][holamundo]

Hasta aquí ya podemos desarrollar aplicaciones y testearlas de manera local, haré una segunda parte incluyendo más detalles y lo que es el deploy.

Saludos.

[downloadsdk]: {{ get_asset('images/downloadsdk.png') }}
[zip]: {{ get_asset('images/zip.png') }}
[appyaml]: {{ get_asset('images/appyaml.png') }}
[codepy]: {{ get_asset('images/codepy.png') }}
[holamundo]: {{ get_asset('images/holamundo.png') }}
