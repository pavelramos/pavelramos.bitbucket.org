---
layout: post.html
title: Creando entradas fácilmente con Mynt
tags: [web]
---
Hoy les mostraré un poco acerca de Mynt y la manera de publicar entradas. Como les decía anteriormente, Mynt es un generador de sitios estáticos escrito en Python. Esto quiere decir que tendremos una fuente (source) con la cual generaremos un sitio completo en segundos.

Para lograr ello Mynt cuenta con la siguiente estructura:

<pre><code>assets/
posts/
templates/
archives/
</code></pre>

En la carpeta "assets/" se encuentran todos los archivos que comúnmente irían en una carpeta "static/"; es decir imágenes, hojas de estilo, scripts, etc. En la carpeta "posts/" irán todas las entrada que queramos se publiquen en el blog, la cual usaremos el día de hoy. En "templates/" estará nuestro sistema de plantillas con el diseño del blog. Finalmente, la carpeta "archives/" cumple la misma función que "archivo" de Wordpress, un historial de las entradas del blog.

Si entramos a la carpeta posts, para este blog encontramos:

<pre><code>2013-01-21-Renovando-blog.md
2013-01-24-Rendimiento-IO-VPS.md
</code></pre>

¿Se dieron cuenta?, son las 2 publicaciones con las que cuenta el blog hasta el momento. Aquí viene la simplicidad de Mynt para crear entradas, lo único que tenemos que hacer es crear un nuevo archivo Markdown (.md) con el contenido que queremos que tenga nuestra publicación. Por ejemplo para esta entrada sería:

<pre><code>2013-01-26-Creando-entradas-facilmente-con-mynt.md
</code></pre>

Les comparto una imagen que muestra lo escrito, para ello utilizo _Nano_ en Linux Mint, en Windows pueden usar el editor de su preferencia.

[![Screenshot de escritorio][post-3-t]][post-3-f]

Luego de terminar de editar nuestra entrada, sólo basta generar nuestro sitio con un comando propio de Mynt, en mi caso:

<pre><code>$ mynt gen -f src public
</code></pre>

¡Y listo!, tenemos nuestra nueva entrada publicada. En estos días publicaré una guía acerca de Mynt, explicando paso por paso sus configuraciones, para quienes tienen problemas para instalarlo y/o usarlo.

[post-3-t]: {{ get_asset('images/post-3-t.png') }}
[post-3-f]:  {{ get_asset('images/post-3-f.png') }}
