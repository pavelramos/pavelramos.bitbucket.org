#!/bin/bash

# Crear archivo post-receive en carpeta /hooks/
# $ chmod +x post-receive
# Por ahora funcional, pendiente de optimizado..

# SETUP
VIRTUAL_ENV=pavelramos.com
APP_NAME=pavelramos.com

# GET BRANCH
#while read oldrev newrev ref
#do
#BRANCH=`echo $ref | cut -d/ -f3`
#done
BRANCH=master

HOME_PATH=/home/apps/
APP_PATH=$HOME_PATH"www/"$APP_NAME"/"
ENV_PATH=$HOME_PATH".virtualenvs/"$VIRTUAL_ENV"/"

# DEPLOY
echo "#################################################"
echo "### Start deploy"
echo "#################################################"

echo "Checkout .."
GIT_WORK_TREE=$APP_PATH git checkout $BRANCH -f

#echo "Installing eggs .."
#pip install --install-option="--prefix=$ENV_PATH" -r $APP_PATH"requiriments.txt"

#echo "Compiling less .."
#lessc $APP_PATH"apps/static/less/bootstrap.less" > $APP_PATH"apps/static/stylesheets/bootstrap.css"

echo "Generando sitio .."
source /home/apps/.virtualenvs/pavelramos.com/bin/activate
cd /home/apps/www/pavelramos.com/
fab build deploy
echo "Eso fue todo :) .."

echo "Touching .."
touch $HOME_PATH"www/reload.wsgi"

echo "#################################################"
echo "### End deploy"
echo "#################################################"